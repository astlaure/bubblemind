@extends('layouts.app')

@section('content')
    <section class="main-section">
        <div class="container">
            <h1 class="mt-0">Select a user</h1>
            <a class="button primary-outlined mb-2" href="#">New User</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Alexandre St-Laurent</td>
                    <td>alexandre@gmail.io</td>
                    <td>ADMIN</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
@endsection
