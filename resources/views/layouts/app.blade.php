<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @yield('head')
</head>
<body class="@yield('body-classes')">

<aside class="sidebar">
    <div class="sidebar-brand">
        <h4 class="text-center">Bubble Mind</h4>
    </div>
    <ul class="sidebar-nav">
        <li><h6>Administration</h6></li>
        <li><a href="{{ route('admin.users') }}">Users</a></li>
        <li><a href="{{ route('admin.websites') }}">Websites</a></li>
        @if(request()->route('slug'))
            <li><h6>Website</h6></li>
            <li><a href="{{ route('website.users', ['slug' => request()->route('slug')]) }}">Users</a></li>
            <li><a href="{{ route('website.media', ['slug' => request()->route('slug')]) }}">Media</a></li>
            <li><a href="{{ route('website.pages', ['slug' => request()->route('slug')]) }}">Pages</a></li>
            <li><a href="{{ route('website.templates', ['slug' => request()->route('slug')]) }}">Templates</a></li>
        @endif
        <li><h6>Entities</h6></li>
        <li><a href="/">Testimonials</a></li>
        <li><a href="/">Team Members</a></li>
    </ul>
</aside>

<div class="content">
    @yield('content')
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
