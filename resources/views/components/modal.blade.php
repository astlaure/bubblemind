<div id="{{ $id }}" class="modal">
    <div class="close-container">
        <button class="close" type="button" onclick="toggleModal('#{{ $id }}')">
            <i></i>
            <i></i>
        </button>
    </div>
    {{ $slot }}
</div>
