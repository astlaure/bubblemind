<script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea#myeditorinstance', // Replace this CSS selector to match the placeholder element for TinyMCE
        plugins: 'code table lists image autoresize link',
        toolbar: 'undo redo | formatselect| bold italic link | alignleft aligncenter alignright | indent outdent | image | bullist numlist | table | code',
        menubar: false,
        // autoresize options
        max_height: 900,
        // image_options
        image_caption: true,
    });
</script>
