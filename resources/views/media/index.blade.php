@extends('layouts.app')

@section('content')
    <section class="main-section">
        <div class="container">
            <h1 class="mt-0">Media manager</h1>
            <button type="button" class="button primary-outlined mb-2" onclick="toggleModal('#media-upload')">Upload a medium</button>
            <div class="row grid">
                <div class="col medium-2">
                    <div class="media-grid-item">
                        <img src="/images/tales-of-arise.jpg" alt="#" onclick="toggleModal('#media-details')">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-modal id="media-upload">
        <h1>Dropzone</h1>
    </x-modal>

    <x-modal id="media-details">
        <div>
            <div class="row">
                <div class="col medium-6 flex align-center">
                    <img src="/images/tales-of-arise.jpg" alt="">
                </div>
                <div class="col medium-6">
                    <form class="form" [formGroup]="detailsForm" (ngSubmit)="onSubmit()">
                        <div class="input">
                            <label for="mimetype">Mimetype</label>
                            <input type="text" name="mimetype" id="mimetype" disabled value="">
                        </div>
                        <div class="input">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" formControlName="description"></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="flex justify-end gap-4">
                        <button class="button primary-outlined" type="button" (click)="editMedium()">Edit</button>
                        <button class="button danger-outlined" type="button" (click)="deleteMedium()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </x-modal>
@endsection
