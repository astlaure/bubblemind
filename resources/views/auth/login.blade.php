@extends('layouts.app')

@section('content')
    <section class="main-section">
        <div class="container">
            <div class="row justify-center">
                <div class="col medium-6">
                    <div class="card py-8 px-4">
                        <div class="card-header"></div>
                        <div class="card-body">
                            <form class="form" action="#" method="post">
                                <div class="input">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" id="username">
                                </div>
                                <div class="input">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password">
                                </div>
                                <div class="inline-input">
                                    <input type="checkbox" name="rememberMe" id="rememberMe">
                                    <label for="rememberMe">Remember Me</label>
                                </div>
                                <div class="button-group justify-end">
                                    <button class="button primary" type="submit">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
