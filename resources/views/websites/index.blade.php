@extends('layouts.app')

@section('content')
    <section class="main-section">
        <div class="container">
            <h1 class="mt-0">Select a website</h1>
            <a class="button primary-outlined mb-2" href="#">New Website</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Url</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><a href="{{ route('website.pages', ['slug' => 'saphirdesign']) }}">Saphir Design</a></td>
                    <td>/public/saphirdesign</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
@endsection
