@extends('layouts.app')

@section('content')
    <section class="main-section">
        <div class="container">
            <h1 class="mt-0">Template list</h1>
            <a class="button primary-outlined mb-2" href="#">Add a section</a>
            <div class="template-section">
                <div class="template-row">
                    <div class="template-column">
                        <p># 12343</p>
                        <p>Navbar</p>
                    </div>
                </div>
                <div class="template-row">
                    <div class="template-column template-container-1/3">
                        <p># 12343</p>
                        <p>Navbar</p>
                    </div>
                    <div class="template-column template-container-1/3">
                        <p># 12343</p>
                        <p>Navbar</p>
                    </div>
                    <div class="template-column template-container-1/3">
                        <p># 12343</p>
                        <p>Navbar</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
