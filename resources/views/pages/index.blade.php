@extends('layouts.app')

@section('content')
    <section class="main-section">
        <div class="container">
            <h1 class="mt-0">Select a page</h1>
            <a class="button primary-outlined mb-2" href="{{ route('website.pages.create', ['slug' => request()->route('slug')]) }}">New Page</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Template</th>
                    <th>Slug</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Home</td>
                    <td>Main Template</td>
                    <td>/</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
@endsection
