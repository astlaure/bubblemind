@extends('layouts.app')

@section('head')
    <x-tinymce.config />
@endsection

@section('content')
    <section class="large-section">
        <div class="container">
            <h1 class="mt-0">Create a page</h1>
            <div class="row">
                <div class="col medium-8">
                    <form class="form" action="#">
                        <div class="row">
                            <div class="col medium-6">
                                <div class="input">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" id="title">
                                </div>
                            </div>
                        </div>
                        <div class="input">
                            <label for="content">Content</label>
                            <x-tinymce.editor />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
