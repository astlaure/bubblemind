require('./bootstrap');

window.toggleModal = function (selector) {
    const modal = document.querySelector(selector);

    if (modal) {
        modal.classList.toggle('visible');
    }
}
