<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index() {
        return view('pages.index');
    }

    public function create() {
        return view('pages.create');
    }

    public function save() {
        return view('pages.index');
    }
}
