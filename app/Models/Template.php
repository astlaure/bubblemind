<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperTemplate
 */
class Template extends Model
{
    use HasFactory;

    public function website() {
        return $this->belongsTo(Website::class);
    }

    public function pages() {
        return $this->hasMany(Page::class);
    }
}
