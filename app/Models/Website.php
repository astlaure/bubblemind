<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperWebsite
 */
class Website extends Model
{
    use HasFactory;

    public function pages() {
        return $this->hasMany(Page::class);
    }

    public function templates() {
        return $this->hasMany(Template::class);
    }

    public function media() {
        return $this->hasMany(Medium::class);
    }

    public function users() {
        return $this->hasMany(User::class);
    }
}
