<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/admin/websites');

Route::group(['prefix' => 'admin/users'], function () {
    Route::get('', [\App\Http\Controllers\UserController::class, 'index'])->name('admin.users');
});

Route::group(['prefix' => 'admin/websites'], function () {
    Route::get('', [\App\Http\Controllers\WebsiteController::class, 'index'])->name('admin.websites');
});

Route::redirect('website/{slug}', '/website/{slug}/pages');

Route::group(['prefix' => 'website/{slug}/media'], function () {
    Route::get('', [\App\Http\Controllers\MediumController::class, 'index'])->name('website.media');
});

Route::group(['prefix' => 'website/{slug}/pages'], function () {
    Route::get('', [\App\Http\Controllers\PageController::class, 'index'])->name('website.pages');
    Route::get('create', [\App\Http\Controllers\PageController::class, 'create'])->name('website.pages.create');
});

Route::group(['prefix' => 'website/{slug}/templates'], function () {
    Route::get('', [\App\Http\Controllers\TemplateController::class, 'index'])->name('website.templates');
    Route::get('edit', [\App\Http\Controllers\TemplateController::class, 'edit'])->name('website.templates.edit');
});

Route::group(['prefix' => 'website/{slug}/users'], function () {
    Route::get('', [\App\Http\Controllers\UserController::class, 'index'])->name('website.users');
});
